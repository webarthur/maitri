// Regular Expression Helper
const Rex = module.exports = {

  addReplaceExp (id, regexp, cb, opt) {
    if (typeof regexp === 'string') {
      regexp = new RegExp(regexp, opt)
    }

    this[id] = function (input) {
      return input.replace(regexp, cb)
    }
  },

  addMatchExp () {

  },

  addTestExp () {

  }

}

// Remove file extension
Rex.addReplaceExp('removeExt', /\.[^\.]*$/, '')

// Add "\" to regular expression string
Rex.addReplaceExp('addSlashes', /([^a-z0-9-_+])/gi, '\\$1')

// Get file extension
Rex.addReplaceExp('getExt', /^.*(\..+)$/, '$1')
