const slugify = require('slugify')

module.exports = function (str) {
  return slugify(str.toLowerCase(), {
    remove: /[^a-z0-9 -]/g
  })
}
