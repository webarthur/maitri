// from https://gist.github.com/mojavelinux/2cebc5fd0d2139715f7723f31ba5bfdd
module.exports = async function (str, pattern, callback) {
  var result = ''
  var length = str.length
  var lastIndex = 0
  var index, match

  pattern.lastIndex = 0
  while ((match = pattern.exec(str))) {
    index = match.index
    result += str.slice(lastIndex, index)
    lastIndex = index + match[0].length
    match.push(index, str)
    result += await callback.apply(null, match)
    pattern.lastIndex = lastIndex
    if (!pattern.global) break
    if (lastIndex === index) {
      if (lastIndex === length) break
      pattern.lastIndex = lastIndex++
      result += str.charAt(lastIndex)
    }
  }
  if (lastIndex < length) result += str.slice(lastIndex, length)
  return result
}
