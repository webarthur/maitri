const Schema = require('../schema.js')

const { config } = require('maitri')

module.exports = function (id, filterName = null) {

  // TODO organizar melhor funções entre filtros e scheme id
  // post format filter
  const filters = {

    withPostFormat (req, res, schemaID) {
      if (!req.body.type) {
        res.badRequest({ error: 'Post without type' })
        return null
      }

      const types = {
        ...res.locals.entity.types,
        ...config.defaultTypes
      }

      res.locals.postType = types[req.body.type]
      // console.log(req.body.type, res.locals.postType, res.locals.postType.format)

      if (!res.locals.postType) {
        res.badRequest({ error: 'Erro no formato do post.' })
        return null
      }

      const formatSchemaID = `${schemaID}-${res.locals.postType.format}`

      // Validate if schema exists
      if (Schema.getSchema(formatSchemaID)) {
        return formatSchemaID
      }
      else {
        console.log(`Método sem schema: ${formatSchemaID} formato padrão usado neste caso.`)
        return `${schemaID}-post`
      }
    }

  }

  return function (req, res, next) {
    let schemaID = req.method.toLowerCase() + '-' + id

    // apply filters
    if (filterName)
      schemaID = filters[filterName](req, res, schemaID)

    // type não foi registrado
    if (!schemaID)
      return

    // TODO remover if, obrigar todos a terem schemas
    if (!Schema.getSchema(schemaID)) {
      console.log('Método sem schema: ' + schemaID)
      return next()
    }

    // Validate schema
    Schema.validate(schemaID, req)
      .then(() => next())
      .catch(err => next(err))
  }

}
