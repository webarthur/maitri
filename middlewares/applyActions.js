
module.exports = function (name, obj = {}, cb) {
  const Maitri = require('maitri')
  return function (req, res, next) {
    Maitri.applyActions(name, { ...res.locals, ...obj })
      .then(function () {
        // Apply callback
        cb && cb(req, res)
        next()
      })
  }
}
