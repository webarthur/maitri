// References
// https://blog.restcase.com/rest-api-error-codes-101/
// https://stackoverflow.com/questions/797834/should-a-restful-put-operation-return-something

module.exports = function (req, res, next) {
  res.json.ok = o => res.status(200).json(o || {})
  res.json.created = o => res.status(201).json(o || {})
  res.json.accepted = o => res.status(202).json(o || {})

  // Empty response
  res.json.noContent = o => res.status(204).json(o || {})

  // Form validation
  res.json.badRequest = o => res.status(400).json(o || {})

  // Authentication is required or failed
  res.json.unauthorized = o => res.status(401).json(o || {})
  // * res.unauth()

  // No permissions
  res.json.fobidden = o => res.status(403).json(o || {})

  // Resource not found
  res.json.notFound = o => res.status(403).json(o || {})

  // There is a conflict in the current state of the resource
  res.json.conflict = o => res.status(409).json(o || {})

  // Resource is gone
  res.json.gone = o => res.status(410).json(o || {})

  next()
}
