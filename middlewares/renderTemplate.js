const fs = require('fs')

module.exports = function (name) {
  const Maitri = require('maitri')
  const themePath = Maitri.path('themes/' + Maitri.config.theme)
  const Theme = require(themePath + '/setup.js')

  // Multi Engine render
  return function render (_req, res, next) {
    const template = name || res.locals.scope.template

    // Check if the @ is the first char
    if (template.charAt(0) === '@') {
      // Searh file in each views path stored
      for (views of Maitri.server().settings.multiEngine) {
        const file = `${views.path}/${template.substr(1)}.${views.engine}`

        // Check if file exists
        if (fs.existsSync(file)) {
          Theme.render(file, { scope: res.locals.scope }, function (err, code) {
            if (err) {
              next(err)
              return
            }
            res.send(code)
          })
          break
        }
      }
    }
    // Normal render
    else {
      res.render(template, res.locals.scope)
    }
  }
}
