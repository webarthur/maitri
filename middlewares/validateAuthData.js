const Maitri = require('maitri')

module.exports = function (req, res, next) {
  // TODO melhorar resposta ao frontend
  if (!res.locals.token) {
    if (req.originalUrl === '/api/auth') {
      res.json({})
    }
    else {
      res.json.unauthorized()
    }
    return
  }

  // validate token
  Maitri.validateToken(res.locals.token, function (err, data) {
    if (err) {
      return res.json.unauthorized(err)
    }

    // cache user id
    res.locals.user_id = Maitri.ObjectID(data._id)

    // continue
    next()
  })
}
