module.exports = function (req, res, next) {
  res.locals.opt = {
    entity_id: res.locals.entity._id,
    search: req.query.search,
    limit: req.query.limit,
    skip: req.query.skip,
    sort: { created_at: -1 },
    withCounters: req.query.counters,
  }

  // sort
  if (req.query.sort) {
    const sort = {
      date: 'created_at',
      comments: 'comments_size',
    }
    res.locals.opt.sort = {}
    res.locals.opt.sort[sort[req.query.sort]] = req.query.asc ? 1 : -1

    if (req.query.sort === 'comments') {
      res.locals.opt.sort.created_at = -1
    }
  }

  next()
}
