const Maitri = require('maitri')
const Ajv = require('ajv')
const MongORM = require('mongorm')

const ajv = Maitri.ajv = new Ajv({
  allErrors: true,
  coerceTypes: true,
  useDefaults: true,
  jsonPointers: true
})

Maitri.addSchemas = function (arr) {
  if (typeof arr === 'string') {
    arr = [arr]
  }
  for (let schema of arr) {
    schema.$async = true
    ajv.addSchema(schema)
  }
}

// SETUP VALIDATOR ERRORS
require('ajv-errors')(ajv)

// ADD isNotEmpty KEYWORD
MongORM.schema.addType('isNotEmpty', {
  type: 'string',
  validate: function (_schema, data) {
    return typeof data === 'string' && data.trim() !== ''
  },
  errors: false
})

// ADD isNotEmpty KEYWORD
MongORM.schema.addType('rules', {
  type: 'string',
  compile: function (schema) {
    return (data, _dataPath, parentData, parentDataProperty) => {
      if (typeof schema === 'string') {
        schema = [schema]
      }
      for (rule of schema) {
        if (rule === 'trim') data = data.trim()
        else if (rule === 'lower') data = data.toLowerCase()
        else if (rule === 'upper') data = data.toUpperCase()
        else if (rule === 'capitalize' && data.length > 0) {
          data = data.replace(/^./, data[0].toUpperCase())
        }
      }
      parentData[parentDataProperty] = data
      return true
    }
  }
})

// ADD isNotEmpty KEYWORD
MongORM.schema.addType('someOf', {
  type: 'string',
  compile (schema) {
    return function (data) {
      if (typeof schema === 'string')
        schema = [schema]

      return schema.includes(data)
    }
  }
})

// ADD mongoId KEYWORD
// MongORM.schema.addType('typeDate', {
//   type: 'string',
//   format: 'date-time',
//   compile: function (schema) {
//     return (data, dataPath, parentData, parentDataProperty) => {
//       if (!data) {
//         return true
//       }
//
//       if (typeof data !== 'string') {
//         return false
//       }
//
//       // coerce data
//       parentData[parentDataProperty] = new Date(data)
//
//       return true
//     }
//   }
// })

MongORM.schema.addType('username', {
  type: 'string',
  errors: true,
  validate: function validateTypeNick (_val, dataObject) {
    // validate minLength
    if (dataObject.length < 2) {
      validateTypeNick.errors = [{
        keyword: "minLengthTypeNick",
        message: "Invalid nick format.",
        params: {
          keyword: "minLengthTypeNick"
        }
      }]
      return false
    }

    // validate maxLength
    if (dataObject.length > 22) {
      validateTypeNick.errors = [{
        keyword: "maxLengthTypeNick",
        message: "Invalid nick format.",
        params: {
          keyword: "maxLengthTypeNick"
        }
      }]
      return false
    }

    // validate maxLength
    if (!/^[0-9a-z]*$/.test(dataObject)) {
      validateTypeNick.errors = [{
        keyword: "invalidTypeNick",
        message: "Invalid nick format.",
        params: {
          keyword: "invalidTypeNick"
        }
      }]
      return false
    }

    return true
  }
})

MongORM.schema.addType('password', {
  type: 'string',
  errors: true,
  validate: function validateTypePassword (_val, dataObject) {
    const hasChars = /[a-z]/i.test(dataObject)
    const hasNumbers = /[0-9]/.test(dataObject)
    const hasSpecialChars = /[^a-z0-9 ]/i.test(dataObject)

    // validate minLength
    if (dataObject.length < 8) {
      validateTypePassword.errors = [{
        keyword: "minLengthTypePassword",
        message: "Invalid password format.",
        params: {
          keyword: "minLengthTypePassword"
        }
      }]
      return false
    }

    // validate maxLength
    if (dataObject.length > 60) {
      validateTypePassword.errors = [{
        keyword: "maxLengthTypePassword",
        message: "Invalid password format.",
        params: {
          keyword: "maxLengthTypePassword"
        }
      }]
      return false
    }

    // valdiate ok
    if (hasChars && (hasNumbers || hasSpecialChars)) {
      return true
    }

    validateTypePassword.errors = [{
      keyword: "invalidTypePassword",
      message: "Invalid password format.",
      params: {
        keyword: "invalidTypePassword"
      }
    }]

    return false
  }
})

module.exports = ajv
