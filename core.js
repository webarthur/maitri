// const csrf = require('csurf')
const path = require('path')
const jwt = require('jsonwebtoken')
const express = require('express')
const server = express()

const Maitri = module.exports = {

  absdir: process.cwd() + '/',
  config: null,
  router: express.Router(),
  server: express(),
  express: express,
  models: {},
  middlewares: {
    render: require('./middlewares/renderTemplate.js'),
    applyActions: require('./middlewares/applyActions.js')
  },
  plugins: [],
  filters: {},

  helpers: {
    rex: require('./helpers/rex.js'),
    slugify: require('./helpers/slugify.js'),
    asyncReplace: require('./helpers/asyncReplace.js')
  },

  devEnv: 'dev',

  isDev () {
    return process.env.NODE_ENV.toLowerCase() === this.devEnv
  },

  dev (cb) {
    if (this.isDev()) {
      cb.apply(this)
    }
  },

  path (dir) {
    if (dir[0] !== '/') {
      dir = this.absdir + '/' + dir
    }
    return path.resolve(dir)
  },

  server () {
    return server
  },

  router () {
    return express.Router()
  },

  Router () {
    return express.Router()
  },

  ObjectID (id) {
    const ObjectID = require('mongodb').ObjectID
    return new ObjectID(id)
  },

  load (file) {
    this.plugins.push(this.path(file))
    return this
  },

  setup (config) {
    const fs = require('fs')

    if (typeof config === 'string') {
      const env = process.env.NODE_ENV || ''
      const extraConfig = config.replace(
        /\.json$/,
        '-' + env.toLowerCase() + '.json'
      )
      this.config = require(this.path(config))
      if (fs.existsSync(this.path(extraConfig))) {
        Object.assign(this.config, require(this.path(extraConfig)))
      }
    } else {
      this.config = config
    }

    this.addMiddlewares({
      processQueryOptions: require('./middlewares/processQueryOptions.js'),
      validateSchema: require('./middlewares/validateSchema.js'),
      validateAuthData: require('./middlewares/validateAuthData.js')
    })

    if (this.config.theme) {
      const themePath = Maitri.path('themes/' + this.config.theme)
      const Theme = require(themePath + '/setup.js')
      server.engine(Theme.ext || 'html', Theme.render)
      server.set('views', themePath) // specify the views directory
      server.set('view engine', Theme.ext) // register the template engine
      this.models.Template = Theme
    }

    return this
  },

  messages (file) {
    this.messages = require(this.absdir + file)
    return this
  },

  getToken (data, opt = { expiresIn: '365 days' }) {
    return jwt.sign(data, this.config.session.secret, opt)
  },

  validateToken (token, callback) {
    return jwt.verify(token, this.config.session.secret, callback)
  },

  addViewsPath (path, engine = 'jst', render) {
    const settings = this.server().settings

    if (!settings.views) {
      settings.views = []
    }
    if (typeof settings.views === 'string') {
      settings.views = [settings.views]
    }
    settings.views.push(path)

    // Multiengine
    if (!settings.multiEngine) {
      settings.multiEngine = []
    }
    settings.multiEngine.push({ path, engine, render })

    return this
  },

  // Merge middlewares functions
  addMiddlewares (middlewares) {
    const keys = Object.keys(middlewares)
    for (let key of keys) {
      this.middlewares[key] = middlewares[key]
    }
    return this
  },

  // Add a filter function
  addFilter (name, fn, priority = 10) {
    if (!this.filters[name]) {
      this.filters[name] = []
    }
    this.filters[name].push({fn, priority})
    return this
  },

  addAction (name, fn, priority = 10) {
    return this.addFilter(name, fn, priority)
  },

  // Apply filters functions by name
  applyFilters (name, data) {
    if (!this.filters[name]) {
      return data
    }
    // TODO sort filters
    for (let filter of this.filters[name]) {
      data = filter.fn(data)
    }
    return data
  },

  async applyActions (name, data, cb) {
    if (!this.filters[name]) {
      return Promise.resolve()
    }

    // TODO sort filters
    // TODO force error to debug
    for (let filter of this.filters[name]) {
      var response = filter.fn(data)
      response = await response
    }

    // Trigger callback
    if (cb) {
      cb.apply(this)
    }

    return true
  },

  addModel (name, model) {
    return this.addModels({ [name]: model })
  },

  // Merge models
  addModels (models) {
    for (let key of Object.keys(models)) {
      this.models[key] = models[key]
    }
    return this
  },

  // Merge helpers functions
  addHelpers (helpers) {
    const keys = Object.keys(helpers)
    for (let key of keys) {
      this.helpers[key] = helpers[key]
    }
    return this
  },

  // Starts the application
  start (cb) {
    if (!this.config.port) {
      this.config.port = 8080
    }
    this.applyActions('init', this.config)
      .then(function () {
        require('./server.js')(Maitri)

        // Load plugins
        for (let file of Maitri.plugins) {
          const plugin = require(file)
          if (typeof plugin === 'function') {
            plugin(express, server)
          }
        }

        // Start server
        server.listen(Maitri.config.port)

        // Custom callback
        cb && cb.call(this, Maitri.config)

        // Error Handling
        server.use(function serverErrorHandler (err, req, res, next) {
          // Custom status code; default = 500
          res.status(res.statusCode === 200 ? 500 : res.statusCode)

          // When AJAX request return the response in JSON format
          if (req.xhr || req.headers.accept.indexOf('json')) {
            const response = { error: err.message }

            // Includes the stack when ENV is dev
            if (Maitri.isDev()) {
              response.stack = err.stack
            }

            // Send JSON response
            res.json(response)
          } else {
            next(err)
          }
        })
      })
  }

}
