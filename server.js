module.exports = function (Maitri) {
  const bodyParser = require('body-parser')
  const cookieParser = require('cookie-parser')

  // Call server app reference
  Maitri.server()

    // Express status funtions helpers
    .use(require('./middlewares/expressStatus'))

    // GZip
    .use(require('compression')())

    // Multi engine for templates
    .set('multiEngine', [])

    // Security
    .use(require('helmet')())
    // .disable('x-powered-by')

    // Setup session
    .use(require('express-session')(Maitri.config.session))

    // Setup cookies
    .use(cookieParser())

    // interpreta payload
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ extended: true }))

    // Setup error resopnse function
    // .use(require('express-fail')({csrf: false}))

    // Setup static files
    .use(require('express').static(Maitri.absdir + '/public', {
      // index: 'index.html',
      extensions: ['html'],
      maxage: Maitri.isDev() ? '0' : '2h'
    }))
}
